@given(u'I sign in as a user')
def step_impl(context):
    if "Sign out" not in context.browser.page_source:
        context.browser.get('https://gitlab.com/users/sign_in')
        context.browser.find_element_by_id("user_login").send_keys("tixinepi")
        context.browser.find_element_by_id("user_password").send_keys("tixinepi")
        context.browser.find_element_by_name("commit").click()

        assert "Signed in successfully." in context.browser.page_source

@when(u'I visit new project page')
def step_impl(context):
    context.browser.get('https://gitlab.com/projects/new')

@when(u'fill the project form with valid data')
def step_impl(context):
    context.browser.find_element_by_id("project_path").send_keys("behave-test-tixinepi8")
    context.browser.find_element_by_id("project_visibility_level_20").click()

@when(u'I submit the form')
def step_impl(context):
    context.browser.find_element_by_name("commit").click()

@then(u'I should see the project page')
def step_impl(context):
    assert "Project 'behave-test-tixinepi8' was successfully created" in context.browser.page_source

@then(u'I should see empty project instructions')
def step_impl(context):
    assert "The repository for this project is empty" in context.browser.page_source
    assert "Command line instructions" in context.browser.page_source

@when(u'I visit the edit project page')
def step_impl(context):
    context.browser.get('https://gitlab.com/tixinepi/behave-test-tixinepi8/edit')

@when(u'I remove the project')
def step_impl(context):
    context.browser.find_element_by_xpath("//input[@value='Remove project']").click()
    context.browser.find_element_by_id("confirm_name_input").send_keys("behave-test-tixinepi8")
    context.browser.find_element_by_xpath("//input[@value='Confirm']").click()

@then(u'I should not see the project anymore')
def step_impl(context):
    assert "Project 'behave-test-tixinepi8' will be deleted." in context.browser.page_source

@when(u'I enter the description')
def step_impl(context):
    context.browser.find_element_by_id("project_description").send_keys("My description")

@when(u'I save the project')
def step_impl(context):
    context.browser.find_element_by_xpath("//input[@value='Save changes']").click()

@then(u'I should see the project with a description')
def step_impl(context):
    import time
    time.sleep(5)

    context.browser.get('https://gitlab.com/tixinepi/behave-test-tixinepi8')
    print(context.browser.page_source)
    assert "My description" in context.browser.page_source
