Feature: Project
  In order to get access to project sections
  A user with ability to create a project
  Should be able to create a new one

  Background:
    Given I sign in as a user

  Scenario: User create a project
    When I visit new project page
    And fill the project form with valid data
    And I submit the form
    Then I should see the project page
    And I should see empty project instructions

  @edit
  Scenario: User updating the project description
    When I visit the edit project page
    And I enter the description
    And I save the project
    Then I should see the project with a description

  @delete
  Scenario: User deletes a project
    When I visit the edit project page
    And I remove the project
    Then I should not see the project anymore

