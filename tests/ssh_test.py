import os
import sys
sys.path.insert(0, 'ssh')

import ssh

def test_create_connection():
    result = ssh.create_connection()
    stdin, stdout, stderr =result.exec_command('echo "$USER"')
    assert stdout.read().strip('\n') == os.environ['SSH_USER']

def test_upload_file():
    target_path = "/home/commandemy/log.txt"
    assert ssh.upload_file('log.txt', target_path) == True
    assert ssh.file_exist(target_path) == True

def test_parse_log():
    target_path = "/home/commandemy/log.txt"
    assert "Wed Jun 10 11:00:39 2015 Decided that /dev/rdisk5 is not writable to us" in ssh.file_read(target_path)
    assert "Wed Jun 10 11:05:20 2015 Adding preseed stringd-i network/ssid string Clockwise14" in ssh.file_read(target_path)
    assert "Wed Jun 10 11:05:21 2015 Filesystem is mounted" in ssh.file_read(target_path)

