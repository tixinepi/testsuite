import sys
sys.path.insert(0, 'api')

import api

def test_login():
    response = api.login()
    assert response.status_code == 201
    assert "tixinepi@top1post.ru" in str(response.json())

def test_project_list():
    response = api.project_list()
    assert response.status_code == 200
    assert "python-movies" in str(response.json())

def test_issue_create():
    response = api.issue_create(1661835)
    assert response.status_code == 201

def test_issue_list():
    response = api.issue_list()
    assert response.status_code == 200
    assert "my-issue-" in str(response.json())